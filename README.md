CronkOS
==

Fork of [Paper](https://github.com/PaperMC/Paper) aimed at improving server performance at high playercounts.

CronkOS-API maven dependency:
```xml
<dependency>
    <groupId>com.monstahhh.cronkos</groupId>
    <artifactId>cronkos-api</artifactId>
    <version>1.16.1-R0.1-SNAPSHOT</version>
    <scope>provided</scope>
 </dependency>
 ```

CronkOS-Server maven dependency:
```xml
<dependency>
    <groupId>com.monstahhh.cronkos</groupId>
    <artifactId>cronkos-server</artifactId>
    <version>1.16.1-R0.1-SNAPSHOT</version>
    <scope>provided</scope>
</dependency>
```

There is no repository required since the artifacts should be locally installed
via building CronkOS.

## Building

Requirements:
- You need `git` installed, with a configured user name and email. 
   On windows you need to run from git bash.
- You need `maven` installed
- You need `jdk` 8+ installed to compile (and `jre` 8+ to run)
- Anything else that `paper` requires to build

If all you want is a paperclip server jar, just run `./cr jar`

Otherwise, to setup the `CronkOS-API` and `CronkOS-Server` repo, just run the following command
in your project root `./cr patch` additionally, after you run `./cr patch` you can run `./cr build` to build the 
respective api and server jars.

`./cr patch` should initialize the repo such that you can now start modifying and creating
patches. The folder `cr-API` is the api repo and the `cr-Server` folder
is the server repo and will contain the source files you will modify.

#### Creating a patch
Patches are effectively just commits in either `CronkOS-API` or `CronkOS-Server`.
To create one, just add a commit to either repo and run `./cr rb`, and a
patch will be placed in the patches folder. Modifying commits will also modify its
corresponding patch file.

Everything else is licensed under the MIT license, except when note otherwise.
See https://github.com/starlis/empirecraft and https://github.com/electronicboy/byof
for the license of material used/modified by this project.

### Note

The fork is based off of aikar's EMC framework found [here](https://github.com/starlis/empirecraft)
